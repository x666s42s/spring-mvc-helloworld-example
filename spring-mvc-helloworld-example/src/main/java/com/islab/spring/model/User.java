/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.islab.spring.model;

/**
 *
 * @author user
 */
public class User {
    private String Name;

    public User(String Name) {
        this.Name = Name;
    }

    public User() {
        
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
}
