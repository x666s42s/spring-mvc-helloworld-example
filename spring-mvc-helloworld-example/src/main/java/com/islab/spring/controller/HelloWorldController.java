package com.islab.spring.controller;

import com.islab.spring.dao.UserDAO;
import com.islab.spring.model.User;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author imssbora
 */
@Controller
public class HelloWorldController {
   
    @Autowired
    private UserDAO userDAO;
    
   @RequestMapping(value="/")
   public ModelAndView listUser(ModelAndView model)  throws IOException{
      
      List<User> listUser = userDAO.list();
      
       
      model.addObject("message","Hello Spring MVC!");
     
      //Java 8 LocalDate
      DateTimeFormatter formatter=DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
      LocalDate date=LocalDate.now();
      model.addObject("date", date.format(formatter));
      
      model.addObject("listUser", listUser);
      
      return model;
   }
   
}
