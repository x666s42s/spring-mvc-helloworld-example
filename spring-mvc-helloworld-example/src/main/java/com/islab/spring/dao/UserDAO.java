/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.islab.spring.dao;

import com.islab.spring.model.User;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author user
 */
@Repository
public interface UserDAO {
    public void saveOrUpdate(User user);
     
    public void delete(String userName);
     
    public User get(String userName);
     
    public List<User> list();
}
