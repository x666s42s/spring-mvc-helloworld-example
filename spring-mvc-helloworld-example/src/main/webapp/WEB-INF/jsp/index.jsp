<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Spring 4 MVC - Hello World Example | BORAJI.COM</title>
</head>
<body>
	<h2>${message}</h2>
	<h4>Server date time is : ${date} </h4>
</body>
</html>