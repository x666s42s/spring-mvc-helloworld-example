<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=x-windows-950">
        <title>Doctor List</title>
    </head>
    <body>
        <div align="center">
	        <h1>Doctor List</h1>
                <h3><a href="newDoctor">New Doctor</a></h3>

	        <table border="1">

                     <th>DoctorNo</th>
                     <th>DoctorName</th>


	        	
			    <c:forEach var="doctor" items="${listDoctor}">
                            <tr>

                                <td>${doctor.d_id}</td>
                                <td>${doctor.d_name}</td>



							
                            </tr>
                         </c:forEach>	        	
			</table>
    	</div>
    </body>
</html>
