package com.islab.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;


import javax.sql.DataSource;

import com.islab.spring.model.Data;
import com.islab.spring.model.Doctor;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

/**
 * An implementation of the DataDAO interface.
 * @author www.codejava.net
 *
 */
public class DataDAOImpl implements DataDAO {

        private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	public DataDAOImpl(DataSource dataSource) {
              this.dataSource = dataSource;
	      this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveOrUpdate(Data data) {
		if (data.getP_id() != "") {
			// update
//			String sql = "UPDATE [User]  SET name=?"
//						+ "WHERE id=?";
//			jdbcTemplate.update(sql, user.getName(),  user.getId());
		} else {
			// insert
//			String sql = "INSERT INTO [User] (name)"
//						+ " VALUES (?)";
//			jdbcTemplate.update(sql, user.getName());
		}
		
	}

	@Override
	public void delete(int pId,int dId) {
		String sql = "DELETE FROM Patient  WHERE p_id=? AND d_id=?";
		jdbcTemplate.update(sql, pId, dId);
	}

	@Override
	public List<Data> list() {
           
              SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("getDoctorAndPatient");
              jdbcCall.returningResultSet("datas", new RowMapper<Data>() {

                  @Override
			public Data mapRow(ResultSet rs, int rowNum) throws SQLException {
				Data aData = new Data();
	
				aData.setP_id(rs.getString("p_id"));
				aData.setP_name(rs.getString("p_name"));
                            aData.setD_id(rs.getString("d_id"));
                            aData.setD_name(rs.getString("d_name"));
                            aData.setDate(rs.getString("date"));
                            
				return aData;
			}
              });
              
              List<Data> listData = (List<Data>) jdbcCall.execute().get("datas");
//            String sql = "SELECT * FROM Data";
//		List<User> listUser = jdbcTemplate.query(sql, new RowMapper<User>() {
//
//			@Override
//			public Data mapRow(ResultSet rs, int rowNum) throws SQLException {
//				Data aData = new Data();
//	
//				aData.setId(rs.getInt("id"));
//				aData.setName(rs.getString("name"));
//				
//				return aData;
//			}
//			
//		});		
		return listData;
	}

       @Override
	public Data get(int pId, int dId) {
		String sql = "SELECT * FROM Patient,Doctor  WHERE p_id=" + pId + " AND d_id=" + dId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Data>() {
			@Override
			public Data extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Data data = new Data();
					data.setP_id(rs.getString("p_id"));
					data.setP_name(rs.getString("p_name"));
                                   data.setD_id(rs.getString("d_id"));
					data.setD_name(rs.getString("d_name"));
					return data;
				}	
				return null;
			}
		});
	}

       @Override
       public void saveDoctor(Doctor doctor) {
           if (doctor.getD_id() != "") {
                    // update
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("insertOrUpdateDoctor");
                    SqlParameterSource in = new MapSqlParameterSource().addValue("d_id", doctor.getD_id()).addValue("d_name", doctor.getD_name());
                    jdbcCall.execute(in);
           } else {
                    // insert
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("insertOrUpdateDoctor");
                    SqlParameterSource in = new MapSqlParameterSource().addValue("d_id", "").addValue("d_name", doctor.getD_name());
                    jdbcCall.execute(in);
           }

       }

    @Override
    public List<Doctor> listDoctor() {
        String sql = "SELECT * FROM Doctor";
        List<Doctor> listDoctor = jdbcTemplate.query(sql, new RowMapper<Doctor>() {

                @Override
                public Doctor mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Doctor aDoctor = new Doctor();

                        aDoctor.setD_id(rs.getString("d_id"));
                        aDoctor.setD_name(rs.getString("d_name"));

                        return aDoctor;
                }

        });
                
        return listDoctor;
    }


}
