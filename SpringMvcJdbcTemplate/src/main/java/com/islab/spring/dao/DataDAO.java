package com.islab.spring.dao;

import java.util.List;

import com.islab.spring.model.Data;
import com.islab.spring.model.Doctor;

/**
 * Defines DAO operations for the contact model.
 * @author www.codejava.net
 *
 */
public interface DataDAO {
	
	public void saveOrUpdate(Data data);
        
        public void saveDoctor(Doctor doctor);
	
	public void delete(int pId,int dId);
	
	public Data get(int pId, int dId);
        
	public List<Data> list();
        
        public List<Doctor> listDoctor();
}
