package com.islab.spring.config;

import javax.sql.DataSource;

import com.islab.spring.dao.DataDAO;
import com.islab.spring.dao.DataDAOImpl;
import org.apache.commons.dbcp.BasicDataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages="com.islab.spring")
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter{

       @Bean
	public DataDAO getDataDAO() {
            return new DataDAOImpl(getDataSource());
	}
    
	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Bean
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
		dataSource.setUrl("jdbc:jtds:sqlserver://140.129.26.117:1433;databaseName=testSpring");
		dataSource.setUsername("testSpring");
		dataSource.setPassword("test");
		
		return dataSource;
	}
	
	
}
