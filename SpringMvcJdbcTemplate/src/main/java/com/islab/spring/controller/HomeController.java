package com.islab.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.islab.spring.dao.DataDAO;

import com.islab.spring.model.Data;
import com.islab.spring.model.Doctor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller routes accesses to the application to the appropriate
 * hanlder methods. 
 * @author www.codejava.net
 *
 */
@Controller
public class HomeController {

	@Autowired
	private DataDAO dataDAO;
	
	@RequestMapping(value="/")
	public ModelAndView listData(ModelAndView model) throws IOException{
		List<Data> listData = dataDAO.list();
		model.addObject("listData", listData);
		model.setViewName("index");
		
		return model;
	}
	
	@RequestMapping(value = "/newData", method = RequestMethod.GET)
	public ModelAndView newData(ModelAndView model) {
		Data newData = new Data();
		model.addObject("data", newData);
		model.setViewName("DataForm");
		return model;
	}
	
	@RequestMapping(value = "/saveData", method = RequestMethod.POST)
	public ModelAndView saveData(@ModelAttribute Data data) {
		dataDAO.saveOrUpdate(data);		
		return new ModelAndView("redirect:/");
	}
	
	@RequestMapping(value = "/deleteData", method = RequestMethod.GET)
	public ModelAndView deleteUser(HttpServletRequest request) {
		int pId = Integer.parseInt(request.getParameter("p_id"));
              int dId = Integer.parseInt(request.getParameter("d_id"));
		dataDAO.delete(pId,dId);
		return new ModelAndView("redirect:/");
	}
	
	@RequestMapping(value = "/editData", method = RequestMethod.GET)
	public ModelAndView editData(HttpServletRequest request) {
		int pId = Integer.parseInt(request.getParameter("p_id"));
                int dId = Integer.parseInt(request.getParameter("d_id"));
		Data data = dataDAO.get(pId,dId);
		ModelAndView model = new ModelAndView("DataForm");
		model.addObject("data", data);
		
		return model;
	}
        
        @RequestMapping(value="/listDoctor", method = RequestMethod.GET)
	public ModelAndView listDoctor(ModelAndView model) throws IOException{
		List<Doctor> listDoctor = dataDAO.listDoctor();
		model.addObject("listDoctor", listDoctor);
		model.setViewName("DoctorList");
		
		return model;
	}
        
        
       
       @RequestMapping(value = "/newDoctor", method = RequestMethod.GET)
       public ModelAndView newDoctor(ModelAndView model){
           Doctor newDoctor = new Doctor();
           model.addObject("doctor", newDoctor);
           model.setViewName("DoctorForm");
           
           return model;
       }
       
       @RequestMapping(value = "/saveDoctor", method = RequestMethod.POST)
	public ModelAndView saveDoctor(@ModelAttribute Doctor doctor) {
		dataDAO.saveDoctor(doctor);		
		return new ModelAndView("redirect:/");
	}
}
