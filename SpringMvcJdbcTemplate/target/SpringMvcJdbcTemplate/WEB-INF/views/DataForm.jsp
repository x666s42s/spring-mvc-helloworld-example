<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" pageEncoding="UTF-8">
<title>New/Edit Data</title>

    
</head>
<body>
    
    
    
    
	<div align="center">
		<h1>New/Edit Data</h1>
		<form:form action="saveData" method="post" modelAttribute="data">
		<table>
                    
			
			<tr>
                            <td>Patient ID:</td>
                            
                            <td><input type="text" name="p_id" value=${data.p_id}></td>
                            
                            <td><input type="hidden" name="d_id" value=${data.d_id}></td>
                     </tr>            
                     <tr>
				<td>Patient Name:</td>
				<td><input type="text" name="p_name" ></td>
                     </tr>
                     <tr>
                            <td>Doctor Name:</td>
                            <td><input type="text" name="d_name" ></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Save"></td>
			</tr>
		</table>
		</form:form>
	</div>
</body>
</html>